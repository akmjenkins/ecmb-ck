;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		ratesTop,	
		$window = $(window),
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$rates = $('div.rates'),
		$ratesWrap = $('div.rates-wrap'),
		ratesHeight = $rates.outerHeight(),
		FIXED_CLASS = 'rates-fixed',
		COLLAPSE_RATES_AT = 950,
		TOP_OFFSET = 95; //starts being fixed when it gets this close to the top

	var methods = {
	
		setStartingPosition: function() {
			ratesTop = $rates.offset().top;
		},

		setPosition: function() {

			if(typeof ratesTop === 'undefined') {
				this.setStartingPosition();
			}
			
			var scrollTop = $window.scrollTop();
			if(scrollTop + TOP_OFFSET > ratesTop) {
				$rates
					.addClass(FIXED_CLASS)
					.css({top:TOP_OFFSET});
			} else {
				$rates
					.removeClass(FIXED_CLASS)
					.css({top:'auto'});
			}
		},

		onScroll: function() {
			this.isCollapsed() || this.setPosition();
		},

		isCollapsed: function() {
			return window.innerWidth <= 950;
		},

		onResize: function() {
			if(this.isCollapsed()) {
				$rates
					.removeClass(FIXED_CLASS)
					.css({top:'auto'});
				return;
			}

			this.setPosition();
		}

	};

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));